package co.th.chat

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.google.gson.JsonIOException
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    private var mSocket: Socket? = null
    private var request =
        "{ \"userId\": \"varodom.naul@gmail.com_cus\", roomId: \"7d9316ee-708a-4792-99bd-6a8e51869b7d\" }"
    var chatAdapter = ChatAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val app = ChatApplication()
        mSocket = app.socket
        mSocket?.connect()

        mSocket?.on("roomtestshop12@apimail.com_dis") { args ->
            // println(args[0].toString())
//            val response = Gson().fromJson(args[0].toString(), ConnectChatResponse::class.java)
//            Log.d("jsonResponse", "" + response)
        }

        btn_send.setOnClickListener {
            sendNewMessage(et_text.text.toString())
        }
    }

    private fun connectChat() {
        mSocket?.on("7d9316ee-708a-4792-99bd-6a8e51869b7d") { args ->
            var arrayList: ArrayList<ConnectChatResponse> = arrayListOf()
            var messageModel: ConnectChatResponse
            try {
                val jsonArray = args[0] as JSONArray
                for (n in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.getJSONObject(n)
                    messageModel =
                        Gson().fromJson(jsonObject.toString(), ConnectChatResponse::class.java)
                    arrayList.add(messageModel)
                    //  Log.d("responseModel", "" + messageModel)
                }
            } catch (e: JsonIOException) {
                e.printStackTrace()
            }
            runOnUiThread {
                setChatAdapter(arrayList, "varodom.naul@gmail.com_cus")
            }
        }
        mSocket?.emit("connectChat", JSONObject(request))
        Log.d("connectChatJson", "" + JSONObject(request))
    }

    override fun onStart() {
        super.onStart()
        Log.d("isconnected", "" + mSocket?.connected())
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_chat.layoutManager = layoutManager
        connectChat()
    }

    private fun setChatAdapter(chatList: ArrayList<ConnectChatResponse>, userId: String) {
        chatAdapter.dataList = chatList
        chatAdapter.userId = userId
        chatAdapter.notifyDataSetChanged()
        rv_chat.adapter = chatAdapter
        rv_chat.scrollToPosition(rv_chat.adapter?.itemCount!! - 1)
    }

    private fun sendNewMessage(message: String) {
//        mSocket?.on("IFUNIEUNFISUN-IGUNIRNUGRSG-IGJNIESJNG"){args->
//            println(args[0].toString())
//        }
        val newMessage =
            "{ \"roomId\": \"7d9316ee-708a-4792-99bd-6a8e51869b7d\", \"userId\": \"varodom.naul@gmail.com_cus\", \"username\": \"max\", \"message\": \"$message\", \"type\": \"text\" }"
        mSocket?.emit("new message", JSONObject(newMessage))
        Log.d("sendNewMessage", "" + JSONObject(newMessage))
    }

    override fun onDestroy() {
        super.onDestroy()
        mSocket?.disconnect()
        mSocket?.off("roomtestshop12@apimail.com_dis")
        mSocket?.off("7d9316ee-708a-4792-99bd-6a8e51869b7d")
    }

}
