package co.th.chat

import android.app.Application
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException


class ChatApplication : Application() {

    var socket: Socket? = null
        private set

    init {
        try {
            //socket = IO.socket("https://socket-io-chat.now.sh/")
            socket = IO.socket("http://103.74.254.101")
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    override fun onCreate() {
        super.onCreate()
    }
}
