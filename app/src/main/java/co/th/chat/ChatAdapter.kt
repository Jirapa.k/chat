package co.th.chat

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.th.chat.databinding.ChatItemBinding

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    var dataList: ArrayList<ConnectChatResponse> = arrayListOf()
    var userId = ""

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ChatItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.chat_item, parent, false)
        return ViewHolder(binding, userId)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("dataList", "" + dataList)
        holder.bindItem(dataList[position])
        //holder.itemView.btn_view_more.setOnClickListener { onClick(dataList[position]) }
    }

    class ViewHolder(private val binding: ChatItemBinding, private val userId: String) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: ConnectChatResponse) {
            binding.model = data
//            if (data.userId == userId) {
//                binding.ivDis.visibility = View.INVISIBLE
//                binding.disMessage.visibility = View.INVISIBLE
//            } else {
//                binding.ivCus.visibility = View.INVISIBLE
//                binding.cusMessage.visibility = View.INVISIBLE
//            }
        }
    }
}