package co.th.chat

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ConnectChatResponse(
    @SerializedName("profileUrl")
    @Expose
    var profileUrl: String = "",

    @SerializedName("roomId")
    @Expose
    var roomId: String = "",

    @SerializedName("userId")
    @Expose
    var userId: String = "",

    @SerializedName("username")
    @Expose
    var username: String = "",

    @SerializedName("message")
    @Expose
    var message: String = "",

    @SerializedName("type")
    @Expose
    var type: String = "",

    @SerializedName("create")
    @Expose
    var create: String = "",

    @SerializedName("createId")
    @Expose
    var createId: String = "",

    @SerializedName("time")
    @Expose
    var time: String = ""
)